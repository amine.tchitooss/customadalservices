import { Injectable, Inject, NgModule, defineInjectable, inject as inject$1 } from '@angular/core';
import { bindCallback } from 'rxjs';
import { inject } from 'adal-angular';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var MsAdalAngular6Service = /** @class */ (function () {
    function MsAdalAngular6Service(adalConfig) {
        this.adalConfig = adalConfig;
        if (typeof adalConfig === 'function') {
            this.adalConfig = adalConfig();
        }
        this.context = inject(this.adalConfig);
        this.handleCallback();
    }
    Object.defineProperty(MsAdalAngular6Service.prototype, "LoggedInUserEmail", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.isAuthenticated) {
                return this.context.getCachedUser().userName;
            }
            return '';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MsAdalAngular6Service.prototype, "LoggedInUserName", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.isAuthenticated) {
                return this.context.getCachedUser().profile.name;
            }
            return '';
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    MsAdalAngular6Service.prototype.login = /**
     * @return {?}
     */
    function () {
        this.context.login();
    };

    MsAdalAngular6Service.prototype.login_userhint = /**
        * @return {?}
        */
    function (loginid, pType) {
        if(pType){
            this.context.config.extraQueryParameter = 'login_hint=' + loginid;
            this.context.login();
        }else{
            this.context.config.logOutUri="https://sso-test.servier.com/adfs/oauth2/logout?post_logout_redirect_uri="+window.location.origin+"&id_token_hint="+loginid
            this.context.logOut();
        }
    };
    /**
     * @return {?}
     */
    MsAdalAngular6Service.prototype.logout = /**
     * @return {?}
     */
    function () {
        this.context.logOut();
    };
    /**
     * @param {?} url
     * @return {?}
     */
    MsAdalAngular6Service.prototype.GetResourceForEndpoint = /**
     * @param {?} url
     * @return {?}
     */
    function (url) {
        var /** @type {?} */ resource = null;
        if (url) {
            resource = this.context.getResourceForEndpoint(url);
            if (!resource) {
                resource = this.adalConfig.clientId;
            }
        }
        return resource;
    };
    /**
     * @param {?} url
     * @return {?}
     */
    MsAdalAngular6Service.prototype.RenewToken = /**
     * @param {?} url
     * @return {?}
     */
    function (url) {
        var /** @type {?} */ resource = this.GetResourceForEndpoint(url);
        return this.context.clearCacheForResource(resource); // Trigger the ADAL token renew
    };
    /**
     * @param {?} url
     * @return {?}
     */
    MsAdalAngular6Service.prototype.acquireToken = /**
     * @param {?} url
     * @return {?}
     */
    function (url) {
        var /** @type {?} */ _this = this; // save outer this for inner function
        var /** @type {?} */ errorMessage;
        return bindCallback(acquireTokenInternal, function (token) {
            if (!token && errorMessage) {
                throw (errorMessage);
            }
            return token;
        })();
        /**
         * @param {?} cb
         * @return {?}
         */
        function acquireTokenInternal(cb) {
            var /** @type {?} */ s = null;
            var /** @type {?} */ resource;
            resource = _this.GetResourceForEndpoint(url);
            _this.context.acquireToken(resource, function (error, tokenOut) {
                if (error) {
                    _this.context.error('Error when acquiring token for resource: ' + resource, error);
                    errorMessage = error;
                    cb(/** @type {?} */ (null));
                }
                else {
                    cb(tokenOut);
                    s = tokenOut;
                }
            });
            return s;
        }
    };
    /**
     * @param {?} url
     * @return {?}
     */
    MsAdalAngular6Service.prototype.getToken = /**
     * @param {?} url
     * @return {?}
     */
    function (url) {
        var /** @type {?} */ resource = this.context.getResourceForEndpoint(url);
        var /** @type {?} */ storage = this.adalConfig.cacheLocation;
        var /** @type {?} */ key;
        if (resource) {
            key = 'adal.access.token.key' + resource;
        }
        else {
            key = 'adal.idtoken';
        }
        if (storage === 'localStorage') {
            return localStorage.getItem(key);
        }
        else {
            return sessionStorage.getItem(key);
        }
    };
    /**
     * @return {?}
     */
    MsAdalAngular6Service.prototype.handleCallback = /**
     * @return {?}
     */
    function () {
        this.context.handleWindowCallback();
    };
    Object.defineProperty(MsAdalAngular6Service.prototype, "userInfo", {
        get: /**
         * @return {?}
         */
        function () {
            return this.context.getCachedUser();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MsAdalAngular6Service.prototype, "accessToken", {
        get: /**
         * @return {?}
         */
        function () {
            return this.context.getCachedToken(this.adalConfig.clientId);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MsAdalAngular6Service.prototype, "isAuthenticated", {
        get: /**
         * @return {?}
         */
        function () {
            return (this.userInfo && this.accessToken) ? true : false;
        },
        enumerable: true,
        configurable: true
    });
    MsAdalAngular6Service.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] },
    ];
    /** @nocollapse */
    MsAdalAngular6Service.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: ['adalConfig',] },] },
    ]; };
    /** @nocollapse */ MsAdalAngular6Service.ngInjectableDef = defineInjectable({ factory: function MsAdalAngular6Service_Factory() { return new MsAdalAngular6Service(inject$1("adalConfig")); }, token: MsAdalAngular6Service, providedIn: "root" });
    return MsAdalAngular6Service;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var AuthenticationGuard = /** @class */ (function () {
    function AuthenticationGuard(adalSvc) {
        this.adalSvc = adalSvc;
    }
    /**
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    AuthenticationGuard.prototype.canActivate = /**
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    function (route, state) {
        if (this.adalSvc.isAuthenticated) {
            return true;
        }
        else {
            this.adalSvc.login();
            return false;
        }
    };
    /**
     * @param {?} childRoute
     * @param {?} state
     * @return {?}
     */
    AuthenticationGuard.prototype.canActivateChild = /**
     * @param {?} childRoute
     * @param {?} state
     * @return {?}
     */
    function (childRoute, state) {
        return this.canActivate(childRoute, state);
    };
    AuthenticationGuard.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    AuthenticationGuard.ctorParameters = function () { return [
        { type: MsAdalAngular6Service, },
    ]; };
    return AuthenticationGuard;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var MsAdalAngular6Module = /** @class */ (function () {
    function MsAdalAngular6Module() {
    }
    /**
     * @param {?} adalConfig
     * @return {?}
     */
    MsAdalAngular6Module.forRoot = /**
     * @param {?} adalConfig
     * @return {?}
     */
    function (adalConfig) {
        return {
            ngModule: MsAdalAngular6Module,
            providers: [MsAdalAngular6Service, { provide: 'adalConfig', useValue: adalConfig }]
        };
    };
    MsAdalAngular6Module.decorators = [
        { type: NgModule, args: [{
                    imports: [],
                    declarations: [],
                    exports: []
                },] },
    ];
    return MsAdalAngular6Module;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { MsAdalAngular6Service, AuthenticationGuard, MsAdalAngular6Module };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWljcm9zb2Z0LWFkYWwtYW5ndWxhcjYuanMubWFwIiwic291cmNlcyI6WyJuZzovL21pY3Jvc29mdC1hZGFsLWFuZ3VsYXI2L3NyYy9tcy1hZGFsLWFuZ3VsYXI2LnNlcnZpY2UudHMiLCJuZzovL21pY3Jvc29mdC1hZGFsLWFuZ3VsYXI2L3NyYy9hdXRoZW50aWNhdGlvbi1ndWFyZC50cyIsIm5nOi8vbWljcm9zb2Z0LWFkYWwtYW5ndWxhcjYvc3JjL21zLWFkYWwtYW5ndWxhcjYubW9kdWxlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbIi8vLyA8cmVmZXJlbmNlIHBhdGg9Jy4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL0B0eXBlcy9hZGFsL2luZGV4LmQudHMnLz5cbmltcG9ydCB7IEluamVjdGFibGUsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgYmluZENhbGxiYWNrIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgKiBhcyBhZGFsTGliIGZyb20gJ2FkYWwtYW5ndWxhcic7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIE1zQWRhbEFuZ3VsYXI2U2VydmljZSB7XG4gIHByaXZhdGUgY29udGV4dDogYWRhbC5BdXRoZW50aWNhdGlvbkNvbnRleHQ7XG5cbiAgY29uc3RydWN0b3IoQEluamVjdCgnYWRhbENvbmZpZycpIHByaXZhdGUgYWRhbENvbmZpZzogYW55KSB7XG4gICAgaWYgKHR5cGVvZiBhZGFsQ29uZmlnID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICB0aGlzLmFkYWxDb25maWcgPSBhZGFsQ29uZmlnKCk7XG4gICAgfSBcbiAgICB0aGlzLmNvbnRleHQgPSBhZGFsTGliLmluamVjdCh0aGlzLmFkYWxDb25maWcpO1xuICAgIHRoaXMuaGFuZGxlQ2FsbGJhY2soKTtcbiAgfVxuXG4gIHB1YmxpYyBnZXQgTG9nZ2VkSW5Vc2VyRW1haWwoKSB7XG4gICAgaWYgKHRoaXMuaXNBdXRoZW50aWNhdGVkKSB7XG4gICAgICByZXR1cm4gdGhpcy5jb250ZXh0LmdldENhY2hlZFVzZXIoKS51c2VyTmFtZTtcbiAgICB9XG4gICAgcmV0dXJuICcnO1xuICB9XG5cbiAgcHVibGljIGdldCBMb2dnZWRJblVzZXJOYW1lKCkge1xuICAgIGlmICh0aGlzLmlzQXV0aGVudGljYXRlZCkge1xuICAgICAgcmV0dXJuIHRoaXMuY29udGV4dC5nZXRDYWNoZWRVc2VyKCkucHJvZmlsZS5uYW1lO1xuICAgIH1cbiAgICByZXR1cm4gJyc7XG4gIH1cblxuICBwdWJsaWMgbG9naW4oKSB7XG4gICAgdGhpcy5jb250ZXh0LmxvZ2luKCk7XG4gIH1cblxuICBwdWJsaWMgbG9nb3V0KCkge1xuICAgIHRoaXMuY29udGV4dC5sb2dPdXQoKTtcbiAgfVxuXG4gIHB1YmxpYyBHZXRSZXNvdXJjZUZvckVuZHBvaW50KHVybDogc3RyaW5nKTogc3RyaW5nIHtcbiAgICBsZXQgcmVzb3VyY2UgPSBudWxsO1xuICAgIGlmICh1cmwpIHtcbiAgICAgIHJlc291cmNlID0gdGhpcy5jb250ZXh0LmdldFJlc291cmNlRm9yRW5kcG9pbnQodXJsKTtcbiAgICAgIGlmICghcmVzb3VyY2UpIHtcbiAgICAgICAgcmVzb3VyY2UgPSB0aGlzLmFkYWxDb25maWcuY2xpZW50SWQ7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiByZXNvdXJjZTtcbiAgfVxuXG4gIHB1YmxpYyBSZW5ld1Rva2VuKHVybDogc3RyaW5nKSB7XG4gICAgbGV0IHJlc291cmNlID0gdGhpcy5HZXRSZXNvdXJjZUZvckVuZHBvaW50KHVybCk7XG4gICAgcmV0dXJuIHRoaXMuY29udGV4dC5jbGVhckNhY2hlRm9yUmVzb3VyY2UocmVzb3VyY2UpOyAvLyBUcmlnZ2VyIHRoZSBBREFMIHRva2VuIHJlbmV3IFxuICB9XG5cbiAgcHVibGljIGFjcXVpcmVUb2tlbih1cmw6IHN0cmluZykge1xuICAgIGNvbnN0IF90aGlzID0gdGhpczsgICAvLyBzYXZlIG91dGVyIHRoaXMgZm9yIGlubmVyIGZ1bmN0aW9uXG4gICAgbGV0IGVycm9yTWVzc2FnZTogc3RyaW5nO1xuXG4gICAgcmV0dXJuIGJpbmRDYWxsYmFjayhhY3F1aXJlVG9rZW5JbnRlcm5hbCwgKHRva2VuOiBzdHJpbmcpID0+IHtcbiAgICAgIGlmICghdG9rZW4gJiYgZXJyb3JNZXNzYWdlKSB7XG4gICAgICAgIHRocm93IChlcnJvck1lc3NhZ2UpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHRva2VuO1xuICAgIH0pKCk7XG5cbiAgICBmdW5jdGlvbiBhY3F1aXJlVG9rZW5JbnRlcm5hbChjYjogYW55KSB7XG4gICAgICBsZXQgczogc3RyaW5nID0gbnVsbDtcbiAgICAgIGxldCByZXNvdXJjZTogc3RyaW5nO1xuICAgICAgcmVzb3VyY2UgPSBfdGhpcy5HZXRSZXNvdXJjZUZvckVuZHBvaW50KHVybCk7XG5cbiAgICAgIF90aGlzLmNvbnRleHQuYWNxdWlyZVRva2VuKHJlc291cmNlLCAoZXJyb3I6IHN0cmluZywgdG9rZW5PdXQ6IHN0cmluZykgPT4ge1xuICAgICAgICBpZiAoZXJyb3IpIHtcbiAgICAgICAgICBfdGhpcy5jb250ZXh0LmVycm9yKCdFcnJvciB3aGVuIGFjcXVpcmluZyB0b2tlbiBmb3IgcmVzb3VyY2U6ICcgKyByZXNvdXJjZSwgZXJyb3IpO1xuICAgICAgICAgIGVycm9yTWVzc2FnZSA9IGVycm9yO1xuICAgICAgICAgIGNiKG51bGwgYXMgc3RyaW5nKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBjYih0b2tlbk91dCk7XG4gICAgICAgICAgcyA9IHRva2VuT3V0O1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICAgIHJldHVybiBzO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBnZXRUb2tlbih1cmw6IHN0cmluZyk6IHN0cmluZyB7XG5cbiAgICBjb25zdCByZXNvdXJjZSA9IHRoaXMuY29udGV4dC5nZXRSZXNvdXJjZUZvckVuZHBvaW50KHVybCk7XG4gICAgY29uc3Qgc3RvcmFnZSA9IHRoaXMuYWRhbENvbmZpZy5jYWNoZUxvY2F0aW9uO1xuICAgIGxldCBrZXk7XG4gICAgaWYgKHJlc291cmNlKSB7XG4gICAgICBrZXkgPSAnYWRhbC5hY2Nlc3MudG9rZW4ua2V5JyArIHJlc291cmNlO1xuICAgIH0gZWxzZSB7XG4gICAgICBrZXkgPSAnYWRhbC5pZHRva2VuJztcbiAgICB9XG4gICAgaWYgKHN0b3JhZ2UgPT09ICdsb2NhbFN0b3JhZ2UnKSB7XG4gICAgICByZXR1cm4gbG9jYWxTdG9yYWdlLmdldEl0ZW0oa2V5KTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oa2V5KTtcbiAgICB9XG4gIH1cblxuICBoYW5kbGVDYWxsYmFjaygpIHtcbiAgICB0aGlzLmNvbnRleHQuaGFuZGxlV2luZG93Q2FsbGJhY2soKTtcbiAgfVxuXG4gIHB1YmxpYyBnZXQgdXNlckluZm8oKSB7XG4gICAgcmV0dXJuIHRoaXMuY29udGV4dC5nZXRDYWNoZWRVc2VyKCk7XG4gIH1cblxuICBwdWJsaWMgZ2V0IGFjY2Vzc1Rva2VuKCkge1xuICAgIHJldHVybiB0aGlzLmNvbnRleHQuZ2V0Q2FjaGVkVG9rZW4odGhpcy5hZGFsQ29uZmlnLmNsaWVudElkKTtcbiAgfVxuXG4gIHB1YmxpYyBnZXQgaXNBdXRoZW50aWNhdGVkKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiAodGhpcy51c2VySW5mbyAmJiB0aGlzLmFjY2Vzc1Rva2VuKSA/IHRydWUgOiBmYWxzZTtcbiAgfVxufSIsImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgQ2FuQWN0aXZhdGUsIENhbkFjdGl2YXRlQ2hpbGQsIEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIFJvdXRlclN0YXRlU25hcHNob3QgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgTXNBZGFsQW5ndWxhcjZTZXJ2aWNlIH0gZnJvbSBcIi4vbXMtYWRhbC1hbmd1bGFyNi5zZXJ2aWNlXCI7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBBdXRoZW50aWNhdGlvbkd1YXJkIGltcGxlbWVudHMgQ2FuQWN0aXZhdGUsIENhbkFjdGl2YXRlQ2hpbGQge1xuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYWRhbFN2YzogTXNBZGFsQW5ndWxhcjZTZXJ2aWNlKSB7IH1cblxuICAgIHB1YmxpYyBjYW5BY3RpdmF0ZShyb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3QpOiBib29sZWFuIHtcbiAgICAgICAgaWYgKHRoaXMuYWRhbFN2Yy5pc0F1dGhlbnRpY2F0ZWQpIHtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5hZGFsU3ZjLmxvZ2luKCk7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgY2FuQWN0aXZhdGVDaGlsZChjaGlsZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5jYW5BY3RpdmF0ZShjaGlsZFJvdXRlLCBzdGF0ZSk7XG4gICAgfVxufSIsImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNb2R1bGVXaXRoUHJvdmlkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNc0FkYWxBbmd1bGFyNlNlcnZpY2UgfSBmcm9tICcuL21zLWFkYWwtYW5ndWxhcjYuc2VydmljZSc7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtdLFxuICBkZWNsYXJhdGlvbnM6IFtdLFxuICBleHBvcnRzOiBbXVxufSlcbmV4cG9ydCBjbGFzcyBNc0FkYWxBbmd1bGFyNk1vZHVsZSB7IFxuICBzdGF0aWMgZm9yUm9vdChhZGFsQ29uZmlnOiBhbnkpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcbiAgICByZXR1cm4ge1xuICAgICAgbmdNb2R1bGU6IE1zQWRhbEFuZ3VsYXI2TW9kdWxlLFxuICAgICAgcHJvdmlkZXJzOiBbTXNBZGFsQW5ndWxhcjZTZXJ2aWNlLCB7IHByb3ZpZGU6ICdhZGFsQ29uZmlnJywgdXNlVmFsdWU6IGFkYWxDb25maWcgfV1cbiAgICB9O1xuICB9XG59Il0sIm5hbWVzIjpbImFkYWxMaWIuaW5qZWN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUNBO0lBVUUsK0JBQTBDO1FBQUEsZUFBVSxHQUFWLFVBQVU7UUFDbEQsSUFBSSxPQUFPLFVBQVUsS0FBSyxVQUFVLEVBQUU7WUFDcEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLEVBQUUsQ0FBQztTQUNoQztRQUNELElBQUksQ0FBQyxPQUFPLEdBQUdBLE1BQWMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO0tBQ3ZCOzBCQUVVLG9EQUFpQjs7Ozs7WUFDMUIsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO2dCQUN4QixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLENBQUMsUUFBUSxDQUFDO2FBQzlDO1lBQ0QsT0FBTyxFQUFFLENBQUM7Ozs7OzBCQUdELG1EQUFnQjs7Ozs7WUFDekIsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO2dCQUN4QixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQzthQUNsRDtZQUNELE9BQU8sRUFBRSxDQUFDOzs7Ozs7OztJQUdMLHFDQUFLOzs7O1FBQ1YsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQzs7Ozs7SUFHaEIsc0NBQU07Ozs7UUFDWCxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDOzs7Ozs7SUFHakIsc0RBQXNCOzs7O2NBQUMsR0FBVztRQUN2QyxxQkFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLElBQUksR0FBRyxFQUFFO1lBQ1AsUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDcEQsSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFDYixRQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUM7YUFDckM7U0FDRjtRQUNELE9BQU8sUUFBUSxDQUFDOzs7Ozs7SUFHWCwwQ0FBVTs7OztjQUFDLEdBQVc7UUFDM0IscUJBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNoRCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMscUJBQXFCLENBQUMsUUFBUSxDQUFDLENBQUM7Ozs7OztJQUcvQyw0Q0FBWTs7OztjQUFDLEdBQVc7UUFDN0IscUJBQU0sS0FBSyxHQUFHLElBQUksQ0FBQztRQUNuQixxQkFBSSxZQUFvQixDQUFDO1FBRXpCLE9BQU8sWUFBWSxDQUFDLG9CQUFvQixFQUFFLFVBQUMsS0FBYTtZQUN0RCxJQUFJLENBQUMsS0FBSyxJQUFJLFlBQVksRUFBRTtnQkFDMUIsT0FBTyxZQUFZLEVBQUU7YUFDdEI7WUFDRCxPQUFPLEtBQUssQ0FBQztTQUNkLENBQUMsRUFBRSxDQUFDOzs7OztRQUVMLDhCQUE4QixFQUFPO1lBQ25DLHFCQUFJLENBQUMsR0FBVyxJQUFJLENBQUM7WUFDckIscUJBQUksUUFBZ0IsQ0FBQztZQUNyQixRQUFRLEdBQUcsS0FBSyxDQUFDLHNCQUFzQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRTdDLEtBQUssQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxVQUFDLEtBQWEsRUFBRSxRQUFnQjtnQkFDbkUsSUFBSSxLQUFLLEVBQUU7b0JBQ1QsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsMkNBQTJDLEdBQUcsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUNuRixZQUFZLEdBQUcsS0FBSyxDQUFDO29CQUNyQixFQUFFLG1CQUFDLElBQWMsRUFBQyxDQUFDO2lCQUNwQjtxQkFBTTtvQkFDTCxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ2IsQ0FBQyxHQUFHLFFBQVEsQ0FBQztpQkFDZDthQUNGLENBQUMsQ0FBQztZQUNILE9BQU8sQ0FBQyxDQUFDO1NBQ1Y7Ozs7OztJQUdJLHdDQUFROzs7O2NBQUMsR0FBVztRQUV6QixxQkFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMxRCxxQkFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUM7UUFDOUMscUJBQUksR0FBRyxDQUFDO1FBQ1IsSUFBSSxRQUFRLEVBQUU7WUFDWixHQUFHLEdBQUcsdUJBQXVCLEdBQUcsUUFBUSxDQUFDO1NBQzFDO2FBQU07WUFDTCxHQUFHLEdBQUcsY0FBYyxDQUFDO1NBQ3RCO1FBQ0QsSUFBSSxPQUFPLEtBQUssY0FBYyxFQUFFO1lBQzlCLE9BQU8sWUFBWSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUNsQzthQUFNO1lBQ0wsT0FBTyxjQUFjLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ3BDOzs7OztJQUdILDhDQUFjOzs7SUFBZDtRQUNFLElBQUksQ0FBQyxPQUFPLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztLQUNyQzswQkFFVSwyQ0FBUTs7Ozs7WUFDakIsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxDQUFDOzs7OzswQkFHM0IsOENBQVc7Ozs7O1lBQ3BCLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQzs7Ozs7MEJBR3BELGtEQUFlOzs7OztZQUN4QixPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksR0FBRyxLQUFLLENBQUM7Ozs7OztnQkFoSDdELFVBQVUsU0FBQztvQkFDVixVQUFVLEVBQUUsTUFBTTtpQkFDbkI7Ozs7Z0RBSWMsTUFBTSxTQUFDLFlBQVk7OztnQ0FYbEM7Ozs7Ozs7QUNBQTtJQU1JLDZCQUFvQixPQUE4QjtRQUE5QixZQUFPLEdBQVAsT0FBTyxDQUF1QjtLQUFLOzs7Ozs7SUFFaEQseUNBQVc7Ozs7O2NBQUMsS0FBNkIsRUFBRSxLQUEwQjtRQUN4RSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxFQUFFO1lBQzlCLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7YUFBTTtZQUNILElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDckIsT0FBTyxLQUFLLENBQUM7U0FDaEI7Ozs7Ozs7SUFHRSw4Q0FBZ0I7Ozs7O2NBQUMsVUFBa0MsRUFBRSxLQUEwQjtRQUNsRixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFDOzs7Z0JBZGxELFVBQVU7Ozs7Z0JBRkYscUJBQXFCOzs4QkFGOUI7Ozs7Ozs7QUNBQTs7Ozs7OztJQVVTLDRCQUFPOzs7O0lBQWQsVUFBZSxVQUFlO1FBQzVCLE9BQU87WUFDTCxRQUFRLEVBQUUsb0JBQW9CO1lBQzlCLFNBQVMsRUFBRSxDQUFDLHFCQUFxQixFQUFFLEVBQUUsT0FBTyxFQUFFLFlBQVksRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFLENBQUM7U0FDcEYsQ0FBQztLQUNIOztnQkFYRixRQUFRLFNBQUM7b0JBQ1IsT0FBTyxFQUFFLEVBQUU7b0JBQ1gsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLE9BQU8sRUFBRSxFQUFFO2lCQUNaOzsrQkFSRDs7Ozs7Ozs7Ozs7Ozs7OyJ9