(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('rxjs'), require('adal-angular')) :
    typeof define === 'function' && define.amd ? define('microsoft-adal-angular6', ['exports', '@angular/core', 'rxjs', 'adal-angular'], factory) :
    (factory((global['microsoft-adal-angular6'] = {}),global.ng.core,null,null));
}(this, (function (exports,i0,rxjs,adalLib) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var MsAdalAngular6Service = (function () {
        function MsAdalAngular6Service(adalConfig) {
            this.adalConfig = adalConfig;
            if (typeof adalConfig === 'function') {
                this.adalConfig = adalConfig();
            }
            this.context = adalLib.inject(this.adalConfig);
            this.handleCallback();
        }
        Object.defineProperty(MsAdalAngular6Service.prototype, "LoggedInUserEmail", {
            get: /**
             * @return {?}
             */ function () {
                if (this.isAuthenticated) {
                    return this.context.getCachedUser().userName;
                }
                return '';
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MsAdalAngular6Service.prototype, "LoggedInUserName", {
            get: /**
             * @return {?}
             */ function () {
                if (this.isAuthenticated) {
                    return this.context.getCachedUser().profile.name;
                }
                return '';
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @return {?}
         */
        MsAdalAngular6Service.prototype.login = /**
         * @return {?}
         */
            function () {
                this.context.login();
            };
        /**
         * @return {?}
         */
        MsAdalAngular6Service.prototype.logout = /**
         * @return {?}
         */
            function () {
                this.context.logOut();
            };
        /**
         * @param {?} url
         * @return {?}
         */
        MsAdalAngular6Service.prototype.GetResourceForEndpoint = /**
         * @param {?} url
         * @return {?}
         */
            function (url) {
                var /** @type {?} */ resource = null;
                if (url) {
                    resource = this.context.getResourceForEndpoint(url);
                    if (!resource) {
                        resource = this.adalConfig.clientId;
                    }
                }
                return resource;
            };
        /**
         * @param {?} url
         * @return {?}
         */
        MsAdalAngular6Service.prototype.RenewToken = /**
         * @param {?} url
         * @return {?}
         */
            function (url) {
                var /** @type {?} */ resource = this.GetResourceForEndpoint(url);
                return this.context.clearCacheForResource(resource); // Trigger the ADAL token renew
            };
        /**
         * @param {?} url
         * @return {?}
         */
        MsAdalAngular6Service.prototype.acquireToken = /**
         * @param {?} url
         * @return {?}
         */
            function (url) {
                var /** @type {?} */ _this = this; // save outer this for inner function
                var /** @type {?} */ errorMessage;
                return rxjs.bindCallback(acquireTokenInternal, function (token) {
                    if (!token && errorMessage) {
                        throw (errorMessage);
                    }
                    return token;
                })();
                /**
                 * @param {?} cb
                 * @return {?}
                 */
                function acquireTokenInternal(cb) {
                    var /** @type {?} */ s = null;
                    var /** @type {?} */ resource;
                    resource = _this.GetResourceForEndpoint(url);
                    _this.context.acquireToken(resource, function (error, tokenOut) {
                        if (error) {
                            _this.context.error('Error when acquiring token for resource: ' + resource, error);
                            errorMessage = error;
                            cb(/** @type {?} */ (null));
                        }
                        else {
                            cb(tokenOut);
                            s = tokenOut;
                        }
                    });
                    return s;
                }
            };
        /**
         * @param {?} url
         * @return {?}
         */
        MsAdalAngular6Service.prototype.getToken = /**
         * @param {?} url
         * @return {?}
         */
            function (url) {
                var /** @type {?} */ resource = this.context.getResourceForEndpoint(url);
                var /** @type {?} */ storage = this.adalConfig.cacheLocation;
                var /** @type {?} */ key;
                if (resource) {
                    key = 'adal.access.token.key' + resource;
                }
                else {
                    key = 'adal.idtoken';
                }
                if (storage === 'localStorage') {
                    return localStorage.getItem(key);
                }
                else {
                    return sessionStorage.getItem(key);
                }
            };
        /**
         * @return {?}
         */
        MsAdalAngular6Service.prototype.handleCallback = /**
         * @return {?}
         */
            function () {
                this.context.handleWindowCallback();
            };
        Object.defineProperty(MsAdalAngular6Service.prototype, "userInfo", {
            get: /**
             * @return {?}
             */ function () {
                return this.context.getCachedUser();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MsAdalAngular6Service.prototype, "accessToken", {
            get: /**
             * @return {?}
             */ function () {
                return this.context.getCachedToken(this.adalConfig.clientId);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MsAdalAngular6Service.prototype, "isAuthenticated", {
            get: /**
             * @return {?}
             */ function () {
                return (this.userInfo && this.accessToken) ? true : false;
            },
            enumerable: true,
            configurable: true
        });
        MsAdalAngular6Service.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] },
        ];
        /** @nocollapse */
        MsAdalAngular6Service.ctorParameters = function () {
            return [
                { type: undefined, decorators: [{ type: i0.Inject, args: ['adalConfig',] },] },
            ];
        };
        /** @nocollapse */ MsAdalAngular6Service.ngInjectableDef = i0.defineInjectable({ factory: function MsAdalAngular6Service_Factory() { return new MsAdalAngular6Service(i0.inject("adalConfig")); }, token: MsAdalAngular6Service, providedIn: "root" });
        return MsAdalAngular6Service;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var AuthenticationGuard = (function () {
        function AuthenticationGuard(adalSvc) {
            this.adalSvc = adalSvc;
        }
        /**
         * @param {?} route
         * @param {?} state
         * @return {?}
         */
        AuthenticationGuard.prototype.canActivate = /**
         * @param {?} route
         * @param {?} state
         * @return {?}
         */
            function (route, state) {
                if (this.adalSvc.isAuthenticated) {
                    return true;
                }
                else {
                    this.adalSvc.login();
                    return false;
                }
            };
        /**
         * @param {?} childRoute
         * @param {?} state
         * @return {?}
         */
        AuthenticationGuard.prototype.canActivateChild = /**
         * @param {?} childRoute
         * @param {?} state
         * @return {?}
         */
            function (childRoute, state) {
                return this.canActivate(childRoute, state);
            };
        AuthenticationGuard.decorators = [
            { type: i0.Injectable },
        ];
        /** @nocollapse */
        AuthenticationGuard.ctorParameters = function () {
            return [
                { type: MsAdalAngular6Service, },
            ];
        };
        return AuthenticationGuard;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var MsAdalAngular6Module = (function () {
        function MsAdalAngular6Module() {
        }
        /**
         * @param {?} adalConfig
         * @return {?}
         */
        MsAdalAngular6Module.forRoot = /**
         * @param {?} adalConfig
         * @return {?}
         */
            function (adalConfig) {
                return {
                    ngModule: MsAdalAngular6Module,
                    providers: [MsAdalAngular6Service, { provide: 'adalConfig', useValue: adalConfig }]
                };
            };
        MsAdalAngular6Module.decorators = [
            { type: i0.NgModule, args: [{
                        imports: [],
                        declarations: [],
                        exports: []
                    },] },
        ];
        return MsAdalAngular6Module;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    exports.MsAdalAngular6Service = MsAdalAngular6Service;
    exports.AuthenticationGuard = AuthenticationGuard;
    exports.MsAdalAngular6Module = MsAdalAngular6Module;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWljcm9zb2Z0LWFkYWwtYW5ndWxhcjYudW1kLmpzLm1hcCIsInNvdXJjZXMiOlsibmc6Ly9taWNyb3NvZnQtYWRhbC1hbmd1bGFyNi9zcmMvbXMtYWRhbC1hbmd1bGFyNi5zZXJ2aWNlLnRzIiwibmc6Ly9taWNyb3NvZnQtYWRhbC1hbmd1bGFyNi9zcmMvYXV0aGVudGljYXRpb24tZ3VhcmQudHMiLCJuZzovL21pY3Jvc29mdC1hZGFsLWFuZ3VsYXI2L3NyYy9tcy1hZGFsLWFuZ3VsYXI2Lm1vZHVsZS50cyJdLCJzb3VyY2VzQ29udGVudCI6WyIvLy8gPHJlZmVyZW5jZSBwYXRoPScuLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9AdHlwZXMvYWRhbC9pbmRleC5kLnRzJy8+XG5pbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE9ic2VydmFibGUsIGJpbmRDYWxsYmFjayB9IGZyb20gJ3J4anMnO1xuaW1wb3J0ICogYXMgYWRhbExpYiBmcm9tICdhZGFsLWFuZ3VsYXInO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBNc0FkYWxBbmd1bGFyNlNlcnZpY2Uge1xuICBwcml2YXRlIGNvbnRleHQ6IGFkYWwuQXV0aGVudGljYXRpb25Db250ZXh0O1xuXG4gIGNvbnN0cnVjdG9yKEBJbmplY3QoJ2FkYWxDb25maWcnKSBwcml2YXRlIGFkYWxDb25maWc6IGFueSkge1xuICAgIGlmICh0eXBlb2YgYWRhbENvbmZpZyA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgdGhpcy5hZGFsQ29uZmlnID0gYWRhbENvbmZpZygpO1xuICAgIH0gXG4gICAgdGhpcy5jb250ZXh0ID0gYWRhbExpYi5pbmplY3QodGhpcy5hZGFsQ29uZmlnKTtcbiAgICB0aGlzLmhhbmRsZUNhbGxiYWNrKCk7XG4gIH1cblxuICBwdWJsaWMgZ2V0IExvZ2dlZEluVXNlckVtYWlsKCkge1xuICAgIGlmICh0aGlzLmlzQXV0aGVudGljYXRlZCkge1xuICAgICAgcmV0dXJuIHRoaXMuY29udGV4dC5nZXRDYWNoZWRVc2VyKCkudXNlck5hbWU7XG4gICAgfVxuICAgIHJldHVybiAnJztcbiAgfVxuXG4gIHB1YmxpYyBnZXQgTG9nZ2VkSW5Vc2VyTmFtZSgpIHtcbiAgICBpZiAodGhpcy5pc0F1dGhlbnRpY2F0ZWQpIHtcbiAgICAgIHJldHVybiB0aGlzLmNvbnRleHQuZ2V0Q2FjaGVkVXNlcigpLnByb2ZpbGUubmFtZTtcbiAgICB9XG4gICAgcmV0dXJuICcnO1xuICB9XG5cbiAgcHVibGljIGxvZ2luKCkge1xuICAgIHRoaXMuY29udGV4dC5sb2dpbigpO1xuICB9XG5cbiAgcHVibGljIGxvZ291dCgpIHtcbiAgICB0aGlzLmNvbnRleHQubG9nT3V0KCk7XG4gIH1cblxuICBwdWJsaWMgR2V0UmVzb3VyY2VGb3JFbmRwb2ludCh1cmw6IHN0cmluZyk6IHN0cmluZyB7XG4gICAgbGV0IHJlc291cmNlID0gbnVsbDtcbiAgICBpZiAodXJsKSB7XG4gICAgICByZXNvdXJjZSA9IHRoaXMuY29udGV4dC5nZXRSZXNvdXJjZUZvckVuZHBvaW50KHVybCk7XG4gICAgICBpZiAoIXJlc291cmNlKSB7XG4gICAgICAgIHJlc291cmNlID0gdGhpcy5hZGFsQ29uZmlnLmNsaWVudElkO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gcmVzb3VyY2U7XG4gIH1cblxuICBwdWJsaWMgUmVuZXdUb2tlbih1cmw6IHN0cmluZykge1xuICAgIGxldCByZXNvdXJjZSA9IHRoaXMuR2V0UmVzb3VyY2VGb3JFbmRwb2ludCh1cmwpO1xuICAgIHJldHVybiB0aGlzLmNvbnRleHQuY2xlYXJDYWNoZUZvclJlc291cmNlKHJlc291cmNlKTsgLy8gVHJpZ2dlciB0aGUgQURBTCB0b2tlbiByZW5ldyBcbiAgfVxuXG4gIHB1YmxpYyBhY3F1aXJlVG9rZW4odXJsOiBzdHJpbmcpIHtcbiAgICBjb25zdCBfdGhpcyA9IHRoaXM7ICAgLy8gc2F2ZSBvdXRlciB0aGlzIGZvciBpbm5lciBmdW5jdGlvblxuICAgIGxldCBlcnJvck1lc3NhZ2U6IHN0cmluZztcblxuICAgIHJldHVybiBiaW5kQ2FsbGJhY2soYWNxdWlyZVRva2VuSW50ZXJuYWwsICh0b2tlbjogc3RyaW5nKSA9PiB7XG4gICAgICBpZiAoIXRva2VuICYmIGVycm9yTWVzc2FnZSkge1xuICAgICAgICB0aHJvdyAoZXJyb3JNZXNzYWdlKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB0b2tlbjtcbiAgICB9KSgpO1xuXG4gICAgZnVuY3Rpb24gYWNxdWlyZVRva2VuSW50ZXJuYWwoY2I6IGFueSkge1xuICAgICAgbGV0IHM6IHN0cmluZyA9IG51bGw7XG4gICAgICBsZXQgcmVzb3VyY2U6IHN0cmluZztcbiAgICAgIHJlc291cmNlID0gX3RoaXMuR2V0UmVzb3VyY2VGb3JFbmRwb2ludCh1cmwpO1xuXG4gICAgICBfdGhpcy5jb250ZXh0LmFjcXVpcmVUb2tlbihyZXNvdXJjZSwgKGVycm9yOiBzdHJpbmcsIHRva2VuT3V0OiBzdHJpbmcpID0+IHtcbiAgICAgICAgaWYgKGVycm9yKSB7XG4gICAgICAgICAgX3RoaXMuY29udGV4dC5lcnJvcignRXJyb3Igd2hlbiBhY3F1aXJpbmcgdG9rZW4gZm9yIHJlc291cmNlOiAnICsgcmVzb3VyY2UsIGVycm9yKTtcbiAgICAgICAgICBlcnJvck1lc3NhZ2UgPSBlcnJvcjtcbiAgICAgICAgICBjYihudWxsIGFzIHN0cmluZyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgY2IodG9rZW5PdXQpO1xuICAgICAgICAgIHMgPSB0b2tlbk91dDtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgICByZXR1cm4gcztcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgZ2V0VG9rZW4odXJsOiBzdHJpbmcpOiBzdHJpbmcge1xuXG4gICAgY29uc3QgcmVzb3VyY2UgPSB0aGlzLmNvbnRleHQuZ2V0UmVzb3VyY2VGb3JFbmRwb2ludCh1cmwpO1xuICAgIGNvbnN0IHN0b3JhZ2UgPSB0aGlzLmFkYWxDb25maWcuY2FjaGVMb2NhdGlvbjtcbiAgICBsZXQga2V5O1xuICAgIGlmIChyZXNvdXJjZSkge1xuICAgICAga2V5ID0gJ2FkYWwuYWNjZXNzLnRva2VuLmtleScgKyByZXNvdXJjZTtcbiAgICB9IGVsc2Uge1xuICAgICAga2V5ID0gJ2FkYWwuaWR0b2tlbic7XG4gICAgfVxuICAgIGlmIChzdG9yYWdlID09PSAnbG9jYWxTdG9yYWdlJykge1xuICAgICAgcmV0dXJuIGxvY2FsU3RvcmFnZS5nZXRJdGVtKGtleSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKGtleSk7XG4gICAgfVxuICB9XG5cbiAgaGFuZGxlQ2FsbGJhY2soKSB7XG4gICAgdGhpcy5jb250ZXh0LmhhbmRsZVdpbmRvd0NhbGxiYWNrKCk7XG4gIH1cblxuICBwdWJsaWMgZ2V0IHVzZXJJbmZvKCkge1xuICAgIHJldHVybiB0aGlzLmNvbnRleHQuZ2V0Q2FjaGVkVXNlcigpO1xuICB9XG5cbiAgcHVibGljIGdldCBhY2Nlc3NUb2tlbigpIHtcbiAgICByZXR1cm4gdGhpcy5jb250ZXh0LmdldENhY2hlZFRva2VuKHRoaXMuYWRhbENvbmZpZy5jbGllbnRJZCk7XG4gIH1cblxuICBwdWJsaWMgZ2V0IGlzQXV0aGVudGljYXRlZCgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gKHRoaXMudXNlckluZm8gJiYgdGhpcy5hY2Nlc3NUb2tlbikgPyB0cnVlIDogZmFsc2U7XG4gIH1cbn0iLCJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IENhbkFjdGl2YXRlLCBDYW5BY3RpdmF0ZUNoaWxkLCBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBSb3V0ZXJTdGF0ZVNuYXBzaG90IH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IE1zQWRhbEFuZ3VsYXI2U2VydmljZSB9IGZyb20gXCIuL21zLWFkYWwtYW5ndWxhcjYuc2VydmljZVwiO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQXV0aGVudGljYXRpb25HdWFyZCBpbXBsZW1lbnRzIENhbkFjdGl2YXRlLCBDYW5BY3RpdmF0ZUNoaWxkIHtcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGFkYWxTdmM6IE1zQWRhbEFuZ3VsYXI2U2VydmljZSkgeyB9XG5cbiAgICBwdWJsaWMgY2FuQWN0aXZhdGUocm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIHN0YXRlOiBSb3V0ZXJTdGF0ZVNuYXBzaG90KTogYm9vbGVhbiB7XG4gICAgICAgIGlmICh0aGlzLmFkYWxTdmMuaXNBdXRoZW50aWNhdGVkKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuYWRhbFN2Yy5sb2dpbigpO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIGNhbkFjdGl2YXRlQ2hpbGQoY2hpbGRSb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3QpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY2FuQWN0aXZhdGUoY2hpbGRSb3V0ZSwgc3RhdGUpO1xuICAgIH1cbn0iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTW9kdWxlV2l0aFByb3ZpZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTXNBZGFsQW5ndWxhcjZTZXJ2aWNlIH0gZnJvbSAnLi9tcy1hZGFsLWFuZ3VsYXI2LnNlcnZpY2UnO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXSxcbiAgZGVjbGFyYXRpb25zOiBbXSxcbiAgZXhwb3J0czogW11cbn0pXG5leHBvcnQgY2xhc3MgTXNBZGFsQW5ndWxhcjZNb2R1bGUgeyBcbiAgc3RhdGljIGZvclJvb3QoYWRhbENvbmZpZzogYW55KTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIG5nTW9kdWxlOiBNc0FkYWxBbmd1bGFyNk1vZHVsZSxcbiAgICAgIHByb3ZpZGVyczogW01zQWRhbEFuZ3VsYXI2U2VydmljZSwgeyBwcm92aWRlOiAnYWRhbENvbmZpZycsIHVzZVZhbHVlOiBhZGFsQ29uZmlnIH1dXG4gICAgfTtcbiAgfVxufSJdLCJuYW1lcyI6WyJhZGFsTGliLmluamVjdCIsImJpbmRDYWxsYmFjayIsIkluamVjdGFibGUiLCJJbmplY3QiLCJOZ01vZHVsZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUNBO1FBVUUsK0JBQTBDO1lBQUEsZUFBVSxHQUFWLFVBQVU7WUFDbEQsSUFBSSxPQUFPLFVBQVUsS0FBSyxVQUFVLEVBQUU7Z0JBQ3BDLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxFQUFFLENBQUM7YUFDaEM7WUFDRCxJQUFJLENBQUMsT0FBTyxHQUFHQSxjQUFjLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQy9DLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUN2Qjs4QkFFVSxvREFBaUI7Ozs7Z0JBQzFCLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtvQkFDeEIsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxDQUFDLFFBQVEsQ0FBQztpQkFDOUM7Z0JBQ0QsT0FBTyxFQUFFLENBQUM7Ozs7OzhCQUdELG1EQUFnQjs7OztnQkFDekIsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO29CQUN4QixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztpQkFDbEQ7Z0JBQ0QsT0FBTyxFQUFFLENBQUM7Ozs7Ozs7O1FBR0wscUNBQUs7Ozs7Z0JBQ1YsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQzs7Ozs7UUFHaEIsc0NBQU07Ozs7Z0JBQ1gsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQzs7Ozs7O1FBR2pCLHNEQUFzQjs7OztzQkFBQyxHQUFXO2dCQUN2QyxxQkFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDO2dCQUNwQixJQUFJLEdBQUcsRUFBRTtvQkFDUCxRQUFRLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDcEQsSUFBSSxDQUFDLFFBQVEsRUFBRTt3QkFDYixRQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUM7cUJBQ3JDO2lCQUNGO2dCQUNELE9BQU8sUUFBUSxDQUFDOzs7Ozs7UUFHWCwwQ0FBVTs7OztzQkFBQyxHQUFXO2dCQUMzQixxQkFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNoRCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMscUJBQXFCLENBQUMsUUFBUSxDQUFDLENBQUM7Ozs7OztRQUcvQyw0Q0FBWTs7OztzQkFBQyxHQUFXO2dCQUM3QixxQkFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDO2dCQUNuQixxQkFBSSxZQUFvQixDQUFDO2dCQUV6QixPQUFPQyxpQkFBWSxDQUFDLG9CQUFvQixFQUFFLFVBQUMsS0FBYTtvQkFDdEQsSUFBSSxDQUFDLEtBQUssSUFBSSxZQUFZLEVBQUU7d0JBQzFCLE9BQU8sWUFBWSxFQUFFO3FCQUN0QjtvQkFDRCxPQUFPLEtBQUssQ0FBQztpQkFDZCxDQUFDLEVBQUUsQ0FBQzs7Ozs7Z0JBRUwsOEJBQThCLEVBQU87b0JBQ25DLHFCQUFJLENBQUMsR0FBVyxJQUFJLENBQUM7b0JBQ3JCLHFCQUFJLFFBQWdCLENBQUM7b0JBQ3JCLFFBQVEsR0FBRyxLQUFLLENBQUMsc0JBQXNCLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBRTdDLEtBQUssQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxVQUFDLEtBQWEsRUFBRSxRQUFnQjt3QkFDbkUsSUFBSSxLQUFLLEVBQUU7NEJBQ1QsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsMkNBQTJDLEdBQUcsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDOzRCQUNuRixZQUFZLEdBQUcsS0FBSyxDQUFDOzRCQUNyQixFQUFFLG1CQUFDLElBQWMsRUFBQyxDQUFDO3lCQUNwQjs2QkFBTTs0QkFDTCxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUM7NEJBQ2IsQ0FBQyxHQUFHLFFBQVEsQ0FBQzt5QkFDZDtxQkFDRixDQUFDLENBQUM7b0JBQ0gsT0FBTyxDQUFDLENBQUM7aUJBQ1Y7Ozs7OztRQUdJLHdDQUFROzs7O3NCQUFDLEdBQVc7Z0JBRXpCLHFCQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUMxRCxxQkFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUM7Z0JBQzlDLHFCQUFJLEdBQUcsQ0FBQztnQkFDUixJQUFJLFFBQVEsRUFBRTtvQkFDWixHQUFHLEdBQUcsdUJBQXVCLEdBQUcsUUFBUSxDQUFDO2lCQUMxQztxQkFBTTtvQkFDTCxHQUFHLEdBQUcsY0FBYyxDQUFDO2lCQUN0QjtnQkFDRCxJQUFJLE9BQU8sS0FBSyxjQUFjLEVBQUU7b0JBQzlCLE9BQU8sWUFBWSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDbEM7cUJBQU07b0JBQ0wsT0FBTyxjQUFjLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUNwQzs7Ozs7UUFHSCw4Q0FBYzs7O1lBQWQ7Z0JBQ0UsSUFBSSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO2FBQ3JDOzhCQUVVLDJDQUFROzs7O2dCQUNqQixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLENBQUM7Ozs7OzhCQUczQiw4Q0FBVzs7OztnQkFDcEIsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDOzs7Ozs4QkFHcEQsa0RBQWU7Ozs7Z0JBQ3hCLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQzs7Ozs7O29CQWhIN0RDLGFBQVUsU0FBQzt3QkFDVixVQUFVLEVBQUUsTUFBTTtxQkFDbkI7Ozs7O3dEQUljQyxTQUFNLFNBQUMsWUFBWTs7OztvQ0FYbEM7Ozs7Ozs7QUNBQTtRQU1JLDZCQUFvQixPQUE4QjtZQUE5QixZQUFPLEdBQVAsT0FBTyxDQUF1QjtTQUFLOzs7Ozs7UUFFaEQseUNBQVc7Ozs7O3NCQUFDLEtBQTZCLEVBQUUsS0FBMEI7Z0JBQ3hFLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLEVBQUU7b0JBQzlCLE9BQU8sSUFBSSxDQUFDO2lCQUNmO3FCQUFNO29CQUNILElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQ3JCLE9BQU8sS0FBSyxDQUFDO2lCQUNoQjs7Ozs7OztRQUdFLDhDQUFnQjs7Ozs7c0JBQUMsVUFBa0MsRUFBRSxLQUEwQjtnQkFDbEYsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQzs7O29CQWRsREQsYUFBVTs7Ozs7d0JBRkYscUJBQXFCOzs7a0NBRjlCOzs7Ozs7O0FDQUE7Ozs7Ozs7UUFVUyw0QkFBTzs7OztZQUFkLFVBQWUsVUFBZTtnQkFDNUIsT0FBTztvQkFDTCxRQUFRLEVBQUUsb0JBQW9CO29CQUM5QixTQUFTLEVBQUUsQ0FBQyxxQkFBcUIsRUFBRSxFQUFFLE9BQU8sRUFBRSxZQUFZLEVBQUUsUUFBUSxFQUFFLFVBQVUsRUFBRSxDQUFDO2lCQUNwRixDQUFDO2FBQ0g7O29CQVhGRSxXQUFRLFNBQUM7d0JBQ1IsT0FBTyxFQUFFLEVBQUU7d0JBQ1gsWUFBWSxFQUFFLEVBQUU7d0JBQ2hCLE9BQU8sRUFBRSxFQUFFO3FCQUNaOzttQ0FSRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzsifQ==