/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { NgModule } from '@angular/core';
import { MsAdalAngular6Service } from './ms-adal-angular6.service';
export class MsAdalAngular6Module {
    /**
     * @param {?} adalConfig
     * @return {?}
     */
    static forRoot(adalConfig) {
        return {
            ngModule: MsAdalAngular6Module,
            providers: [MsAdalAngular6Service, { provide: 'adalConfig', useValue: adalConfig }]
        };
    }
}
MsAdalAngular6Module.decorators = [
    { type: NgModule, args: [{
                imports: [],
                declarations: [],
                exports: []
            },] },
];
function MsAdalAngular6Module_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    MsAdalAngular6Module.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    MsAdalAngular6Module.ctorParameters;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXMtYWRhbC1hbmd1bGFyNi5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9taWNyb3NvZnQtYWRhbC1hbmd1bGFyNi8iLCJzb3VyY2VzIjpbInNyYy9tcy1hZGFsLWFuZ3VsYXI2Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUV6QyxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQU9uRSxNQUFNOzs7OztJQUNKLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBZTtRQUM1QixNQUFNLENBQUM7WUFDTCxRQUFRLEVBQUUsb0JBQW9CO1lBQzlCLFNBQVMsRUFBRSxDQUFDLHFCQUFxQixFQUFFLEVBQUUsT0FBTyxFQUFFLFlBQVksRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFLENBQUM7U0FDcEYsQ0FBQztLQUNIOzs7WUFYRixRQUFRLFNBQUM7Z0JBQ1IsT0FBTyxFQUFFLEVBQUU7Z0JBQ1gsWUFBWSxFQUFFLEVBQUU7Z0JBQ2hCLE9BQU8sRUFBRSxFQUFFO2FBQ1oiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTW9kdWxlV2l0aFByb3ZpZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTXNBZGFsQW5ndWxhcjZTZXJ2aWNlIH0gZnJvbSAnLi9tcy1hZGFsLWFuZ3VsYXI2LnNlcnZpY2UnO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXSxcbiAgZGVjbGFyYXRpb25zOiBbXSxcbiAgZXhwb3J0czogW11cbn0pXG5leHBvcnQgY2xhc3MgTXNBZGFsQW5ndWxhcjZNb2R1bGUgeyBcbiAgc3RhdGljIGZvclJvb3QoYWRhbENvbmZpZzogYW55KTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIG5nTW9kdWxlOiBNc0FkYWxBbmd1bGFyNk1vZHVsZSxcbiAgICAgIHByb3ZpZGVyczogW01zQWRhbEFuZ3VsYXI2U2VydmljZSwgeyBwcm92aWRlOiAnYWRhbENvbmZpZycsIHVzZVZhbHVlOiBhZGFsQ29uZmlnIH1dXG4gICAgfTtcbiAgfVxufSJdfQ==