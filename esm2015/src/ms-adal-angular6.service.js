/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable, Inject } from '@angular/core';
import { bindCallback } from 'rxjs';
import * as adalLib from 'adal-angular';
import * as i0 from "@angular/core";
export class MsAdalAngular6Service {
    /**
     * @param {?} adalConfig
     */
    constructor(adalConfig) {
        this.adalConfig = adalConfig;
        if (typeof adalConfig === 'function') {
            this.adalConfig = adalConfig();
        }
        this.context = adalLib.inject(this.adalConfig);
        this.handleCallback();
    }
    /**
     * @return {?}
     */
    get LoggedInUserEmail() {
        if (this.isAuthenticated) {
            return this.context.getCachedUser().userName;
        }
        return '';
    }
    /**
     * @return {?}
     */
    get LoggedInUserName() {
        if (this.isAuthenticated) {
            return this.context.getCachedUser().profile.name;
        }
        return '';
    }
    /**
     * @return {?}
     */
    login() {
        this.context.login();
    }
    /**
     * @return {?}
     */
    logout() {
        this.context.logOut();
    }
    /**
     * @param {?} url
     * @return {?}
     */
    GetResourceForEndpoint(url) {
        let /** @type {?} */ resource = null;
        if (url) {
            resource = this.context.getResourceForEndpoint(url);
            if (!resource) {
                resource = this.adalConfig.clientId;
            }
        }
        return resource;
    }
    /**
     * @param {?} url
     * @return {?}
     */
    RenewToken(url) {
        let /** @type {?} */ resource = this.GetResourceForEndpoint(url);
        return this.context.clearCacheForResource(resource); // Trigger the ADAL token renew
    }
    /**
     * @param {?} url
     * @return {?}
     */
    acquireToken(url) {
        const /** @type {?} */ _this = this; // save outer this for inner function
        let /** @type {?} */ errorMessage;
        return bindCallback(acquireTokenInternal, (token) => {
            if (!token && errorMessage) {
                throw (errorMessage);
            }
            return token;
        })();
        /**
         * @param {?} cb
         * @return {?}
         */
        function acquireTokenInternal(cb) {
            let /** @type {?} */ s = null;
            let /** @type {?} */ resource;
            resource = _this.GetResourceForEndpoint(url);
            _this.context.acquireToken(resource, (error, tokenOut) => {
                if (error) {
                    _this.context.error('Error when acquiring token for resource: ' + resource, error);
                    errorMessage = error;
                    cb(/** @type {?} */ (null));
                }
                else {
                    cb(tokenOut);
                    s = tokenOut;
                }
            });
            return s;
        }
    }
    /**
     * @param {?} url
     * @return {?}
     */
    getToken(url) {
        const /** @type {?} */ resource = this.context.getResourceForEndpoint(url);
        const /** @type {?} */ storage = this.adalConfig.cacheLocation;
        let /** @type {?} */ key;
        if (resource) {
            key = 'adal.access.token.key' + resource;
        }
        else {
            key = 'adal.idtoken';
        }
        if (storage === 'localStorage') {
            return localStorage.getItem(key);
        }
        else {
            return sessionStorage.getItem(key);
        }
    }
    /**
     * @return {?}
     */
    handleCallback() {
        this.context.handleWindowCallback();
    }
    /**
     * @return {?}
     */
    get userInfo() {
        return this.context.getCachedUser();
    }
    /**
     * @return {?}
     */
    get accessToken() {
        return this.context.getCachedToken(this.adalConfig.clientId);
    }
    /**
     * @return {?}
     */
    get isAuthenticated() {
        return (this.userInfo && this.accessToken) ? true : false;
    }
}
MsAdalAngular6Service.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
MsAdalAngular6Service.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: ['adalConfig',] },] },
];
/** @nocollapse */ MsAdalAngular6Service.ngInjectableDef = i0.defineInjectable({ factory: function MsAdalAngular6Service_Factory() { return new MsAdalAngular6Service(i0.inject("adalConfig")); }, token: MsAdalAngular6Service, providedIn: "root" });
function MsAdalAngular6Service_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    MsAdalAngular6Service.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    MsAdalAngular6Service.ctorParameters;
    /** @type {?} */
    MsAdalAngular6Service.prototype.context;
    /** @type {?} */
    MsAdalAngular6Service.prototype.adalConfig;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXMtYWRhbC1hbmd1bGFyNi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbWljcm9zb2Z0LWFkYWwtYW5ndWxhcjYvIiwic291cmNlcyI6WyJzcmMvbXMtYWRhbC1hbmd1bGFyNi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFDQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQWMsWUFBWSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2hELE9BQU8sS0FBSyxPQUFPLE1BQU0sY0FBYyxDQUFDOztBQUt4QyxNQUFNOzs7O0lBR0osWUFBMEM7UUFBQSxlQUFVLEdBQVYsVUFBVTtRQUNsRCxFQUFFLENBQUMsQ0FBQyxPQUFPLFVBQVUsS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ3JDLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxFQUFFLENBQUM7U0FDaEM7UUFDRCxJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztLQUN2Qjs7OztRQUVVLGlCQUFpQjtRQUMxQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztZQUN6QixNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsQ0FBQyxRQUFRLENBQUM7U0FDOUM7UUFDRCxNQUFNLENBQUMsRUFBRSxDQUFDOzs7OztRQUdELGdCQUFnQjtRQUN6QixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztZQUN6QixNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1NBQ2xEO1FBQ0QsTUFBTSxDQUFDLEVBQUUsQ0FBQzs7Ozs7SUFHTCxLQUFLO1FBQ1YsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQzs7Ozs7SUFHaEIsTUFBTTtRQUNYLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUM7Ozs7OztJQUdqQixzQkFBc0IsQ0FBQyxHQUFXO1FBQ3ZDLHFCQUFJLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDcEIsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNSLFFBQVEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3BELEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDZCxRQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUM7YUFDckM7U0FDRjtRQUNELE1BQU0sQ0FBQyxRQUFRLENBQUM7Ozs7OztJQUdYLFVBQVUsQ0FBQyxHQUFXO1FBQzNCLHFCQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDaEQsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMscUJBQXFCLENBQUMsUUFBUSxDQUFDLENBQUM7Ozs7OztJQUcvQyxZQUFZLENBQUMsR0FBVztRQUM3Qix1QkFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ25CLHFCQUFJLFlBQW9CLENBQUM7UUFFekIsTUFBTSxDQUFDLFlBQVksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLEtBQWEsRUFBRSxFQUFFO1lBQzFELEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLFlBQVksQ0FBQyxDQUFDLENBQUM7Z0JBQzNCLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQzthQUN0QjtZQUNELE1BQU0sQ0FBQyxLQUFLLENBQUM7U0FDZCxDQUFDLEVBQUUsQ0FBQzs7Ozs7UUFFTCw4QkFBOEIsRUFBTztZQUNuQyxxQkFBSSxDQUFDLEdBQVcsSUFBSSxDQUFDO1lBQ3JCLHFCQUFJLFFBQWdCLENBQUM7WUFDckIsUUFBUSxHQUFHLEtBQUssQ0FBQyxzQkFBc0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUU3QyxLQUFLLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxLQUFhLEVBQUUsUUFBZ0IsRUFBRSxFQUFFO2dCQUN2RSxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUNWLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLDJDQUEyQyxHQUFHLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFDbkYsWUFBWSxHQUFHLEtBQUssQ0FBQztvQkFDckIsRUFBRSxtQkFBQyxJQUFjLEVBQUMsQ0FBQztpQkFDcEI7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ04sRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUNiLENBQUMsR0FBRyxRQUFRLENBQUM7aUJBQ2Q7YUFDRixDQUFDLENBQUM7WUFDSCxNQUFNLENBQUMsQ0FBQyxDQUFDO1NBQ1Y7Ozs7OztJQUdJLFFBQVEsQ0FBQyxHQUFXO1FBRXpCLHVCQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzFELHVCQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQztRQUM5QyxxQkFBSSxHQUFHLENBQUM7UUFDUixFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ2IsR0FBRyxHQUFHLHVCQUF1QixHQUFHLFFBQVEsQ0FBQztTQUMxQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sR0FBRyxHQUFHLGNBQWMsQ0FBQztTQUN0QjtRQUNELEVBQUUsQ0FBQyxDQUFDLE9BQU8sS0FBSyxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQy9CLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ2xDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixNQUFNLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUNwQzs7Ozs7SUFHSCxjQUFjO1FBQ1osSUFBSSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO0tBQ3JDOzs7O1FBRVUsUUFBUTtRQUNqQixNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsQ0FBQzs7Ozs7UUFHM0IsV0FBVztRQUNwQixNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQzs7Ozs7UUFHcEQsZUFBZTtRQUN4QixNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Ozs7WUFoSDdELFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7Ozs0Q0FJYyxNQUFNLFNBQUMsWUFBWSIsInNvdXJjZXNDb250ZW50IjpbIi8vLyA8cmVmZXJlbmNlIHBhdGg9Jy4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL0B0eXBlcy9hZGFsL2luZGV4LmQudHMnLz5cbmltcG9ydCB7IEluamVjdGFibGUsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgYmluZENhbGxiYWNrIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgKiBhcyBhZGFsTGliIGZyb20gJ2FkYWwtYW5ndWxhcic7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIE1zQWRhbEFuZ3VsYXI2U2VydmljZSB7XG4gIHByaXZhdGUgY29udGV4dDogYWRhbC5BdXRoZW50aWNhdGlvbkNvbnRleHQ7XG5cbiAgY29uc3RydWN0b3IoQEluamVjdCgnYWRhbENvbmZpZycpIHByaXZhdGUgYWRhbENvbmZpZzogYW55KSB7XG4gICAgaWYgKHR5cGVvZiBhZGFsQ29uZmlnID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICB0aGlzLmFkYWxDb25maWcgPSBhZGFsQ29uZmlnKCk7XG4gICAgfSBcbiAgICB0aGlzLmNvbnRleHQgPSBhZGFsTGliLmluamVjdCh0aGlzLmFkYWxDb25maWcpO1xuICAgIHRoaXMuaGFuZGxlQ2FsbGJhY2soKTtcbiAgfVxuXG4gIHB1YmxpYyBnZXQgTG9nZ2VkSW5Vc2VyRW1haWwoKSB7XG4gICAgaWYgKHRoaXMuaXNBdXRoZW50aWNhdGVkKSB7XG4gICAgICByZXR1cm4gdGhpcy5jb250ZXh0LmdldENhY2hlZFVzZXIoKS51c2VyTmFtZTtcbiAgICB9XG4gICAgcmV0dXJuICcnO1xuICB9XG5cbiAgcHVibGljIGdldCBMb2dnZWRJblVzZXJOYW1lKCkge1xuICAgIGlmICh0aGlzLmlzQXV0aGVudGljYXRlZCkge1xuICAgICAgcmV0dXJuIHRoaXMuY29udGV4dC5nZXRDYWNoZWRVc2VyKCkucHJvZmlsZS5uYW1lO1xuICAgIH1cbiAgICByZXR1cm4gJyc7XG4gIH1cblxuICBwdWJsaWMgbG9naW4oKSB7XG4gICAgdGhpcy5jb250ZXh0LmxvZ2luKCk7XG4gIH1cblxuICBwdWJsaWMgbG9nb3V0KCkge1xuICAgIHRoaXMuY29udGV4dC5sb2dPdXQoKTtcbiAgfVxuXG4gIHB1YmxpYyBHZXRSZXNvdXJjZUZvckVuZHBvaW50KHVybDogc3RyaW5nKTogc3RyaW5nIHtcbiAgICBsZXQgcmVzb3VyY2UgPSBudWxsO1xuICAgIGlmICh1cmwpIHtcbiAgICAgIHJlc291cmNlID0gdGhpcy5jb250ZXh0LmdldFJlc291cmNlRm9yRW5kcG9pbnQodXJsKTtcbiAgICAgIGlmICghcmVzb3VyY2UpIHtcbiAgICAgICAgcmVzb3VyY2UgPSB0aGlzLmFkYWxDb25maWcuY2xpZW50SWQ7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiByZXNvdXJjZTtcbiAgfVxuXG4gIHB1YmxpYyBSZW5ld1Rva2VuKHVybDogc3RyaW5nKSB7XG4gICAgbGV0IHJlc291cmNlID0gdGhpcy5HZXRSZXNvdXJjZUZvckVuZHBvaW50KHVybCk7XG4gICAgcmV0dXJuIHRoaXMuY29udGV4dC5jbGVhckNhY2hlRm9yUmVzb3VyY2UocmVzb3VyY2UpOyAvLyBUcmlnZ2VyIHRoZSBBREFMIHRva2VuIHJlbmV3IFxuICB9XG5cbiAgcHVibGljIGFjcXVpcmVUb2tlbih1cmw6IHN0cmluZykge1xuICAgIGNvbnN0IF90aGlzID0gdGhpczsgICAvLyBzYXZlIG91dGVyIHRoaXMgZm9yIGlubmVyIGZ1bmN0aW9uXG4gICAgbGV0IGVycm9yTWVzc2FnZTogc3RyaW5nO1xuXG4gICAgcmV0dXJuIGJpbmRDYWxsYmFjayhhY3F1aXJlVG9rZW5JbnRlcm5hbCwgKHRva2VuOiBzdHJpbmcpID0+IHtcbiAgICAgIGlmICghdG9rZW4gJiYgZXJyb3JNZXNzYWdlKSB7XG4gICAgICAgIHRocm93IChlcnJvck1lc3NhZ2UpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHRva2VuO1xuICAgIH0pKCk7XG5cbiAgICBmdW5jdGlvbiBhY3F1aXJlVG9rZW5JbnRlcm5hbChjYjogYW55KSB7XG4gICAgICBsZXQgczogc3RyaW5nID0gbnVsbDtcbiAgICAgIGxldCByZXNvdXJjZTogc3RyaW5nO1xuICAgICAgcmVzb3VyY2UgPSBfdGhpcy5HZXRSZXNvdXJjZUZvckVuZHBvaW50KHVybCk7XG5cbiAgICAgIF90aGlzLmNvbnRleHQuYWNxdWlyZVRva2VuKHJlc291cmNlLCAoZXJyb3I6IHN0cmluZywgdG9rZW5PdXQ6IHN0cmluZykgPT4ge1xuICAgICAgICBpZiAoZXJyb3IpIHtcbiAgICAgICAgICBfdGhpcy5jb250ZXh0LmVycm9yKCdFcnJvciB3aGVuIGFjcXVpcmluZyB0b2tlbiBmb3IgcmVzb3VyY2U6ICcgKyByZXNvdXJjZSwgZXJyb3IpO1xuICAgICAgICAgIGVycm9yTWVzc2FnZSA9IGVycm9yO1xuICAgICAgICAgIGNiKG51bGwgYXMgc3RyaW5nKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBjYih0b2tlbk91dCk7XG4gICAgICAgICAgcyA9IHRva2VuT3V0O1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICAgIHJldHVybiBzO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBnZXRUb2tlbih1cmw6IHN0cmluZyk6IHN0cmluZyB7XG5cbiAgICBjb25zdCByZXNvdXJjZSA9IHRoaXMuY29udGV4dC5nZXRSZXNvdXJjZUZvckVuZHBvaW50KHVybCk7XG4gICAgY29uc3Qgc3RvcmFnZSA9IHRoaXMuYWRhbENvbmZpZy5jYWNoZUxvY2F0aW9uO1xuICAgIGxldCBrZXk7XG4gICAgaWYgKHJlc291cmNlKSB7XG4gICAgICBrZXkgPSAnYWRhbC5hY2Nlc3MudG9rZW4ua2V5JyArIHJlc291cmNlO1xuICAgIH0gZWxzZSB7XG4gICAgICBrZXkgPSAnYWRhbC5pZHRva2VuJztcbiAgICB9XG4gICAgaWYgKHN0b3JhZ2UgPT09ICdsb2NhbFN0b3JhZ2UnKSB7XG4gICAgICByZXR1cm4gbG9jYWxTdG9yYWdlLmdldEl0ZW0oa2V5KTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oa2V5KTtcbiAgICB9XG4gIH1cblxuICBoYW5kbGVDYWxsYmFjaygpIHtcbiAgICB0aGlzLmNvbnRleHQuaGFuZGxlV2luZG93Q2FsbGJhY2soKTtcbiAgfVxuXG4gIHB1YmxpYyBnZXQgdXNlckluZm8oKSB7XG4gICAgcmV0dXJuIHRoaXMuY29udGV4dC5nZXRDYWNoZWRVc2VyKCk7XG4gIH1cblxuICBwdWJsaWMgZ2V0IGFjY2Vzc1Rva2VuKCkge1xuICAgIHJldHVybiB0aGlzLmNvbnRleHQuZ2V0Q2FjaGVkVG9rZW4odGhpcy5hZGFsQ29uZmlnLmNsaWVudElkKTtcbiAgfVxuXG4gIHB1YmxpYyBnZXQgaXNBdXRoZW50aWNhdGVkKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiAodGhpcy51c2VySW5mbyAmJiB0aGlzLmFjY2Vzc1Rva2VuKSA/IHRydWUgOiBmYWxzZTtcbiAgfVxufSJdfQ==