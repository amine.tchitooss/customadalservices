import { CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { MsAdalAngular6Service } from "./ms-adal-angular6.service";
export declare class AuthenticationGuard implements CanActivate, CanActivateChild {
    private adalSvc;
    constructor(adalSvc: MsAdalAngular6Service);
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean;
    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean;
}
