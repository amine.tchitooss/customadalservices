/// <reference path="../../../node_modules/@types/adal/index.d.ts" />
import { Observable } from 'rxjs';
export declare class MsAdalAngular6Service {
    private adalConfig;
    public context;
    constructor(adalConfig: any);
    readonly LoggedInUserEmail: string;
    readonly LoggedInUserName: any;
    login(): void;
    logout(): void;
    login_userhint(param:string, type:boolean): void;
    GetResourceForEndpoint(url: string): string;
    RenewToken(url: string): void;
    acquireToken(url: string): Observable<any>;
    getToken(url: string): string;
    handleCallback(): void;
    readonly userInfo: adal.User;
    readonly accessToken: string;
    readonly isAuthenticated: boolean;
}
