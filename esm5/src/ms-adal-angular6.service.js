/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable, Inject } from '@angular/core';
import { bindCallback } from 'rxjs';
import * as adalLib from 'adal-angular';
import * as i0 from "@angular/core";
var MsAdalAngular6Service = /** @class */ (function () {
    function MsAdalAngular6Service(adalConfig) {
        this.adalConfig = adalConfig;
        if (typeof adalConfig === 'function') {
            this.adalConfig = adalConfig();
        }
        this.context = adalLib.inject(this.adalConfig);
        this.handleCallback();
    }
    Object.defineProperty(MsAdalAngular6Service.prototype, "LoggedInUserEmail", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.isAuthenticated) {
                return this.context.getCachedUser().userName;
            }
            return '';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MsAdalAngular6Service.prototype, "LoggedInUserName", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.isAuthenticated) {
                return this.context.getCachedUser().profile.name;
            }
            return '';
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    MsAdalAngular6Service.prototype.login = /**
     * @return {?}
     */
    function () {
        this.context.login();
    };
    /**
     * @return {?}
     */
    MsAdalAngular6Service.prototype.logout = /**
     * @return {?}
     */
    function () {
        this.context.logOut();
    };
    /**
     * @param {?} url
     * @return {?}
     */
    MsAdalAngular6Service.prototype.GetResourceForEndpoint = /**
     * @param {?} url
     * @return {?}
     */
    function (url) {
        var /** @type {?} */ resource = null;
        if (url) {
            resource = this.context.getResourceForEndpoint(url);
            if (!resource) {
                resource = this.adalConfig.clientId;
            }
        }
        return resource;
    };
    /**
     * @param {?} url
     * @return {?}
     */
    MsAdalAngular6Service.prototype.RenewToken = /**
     * @param {?} url
     * @return {?}
     */
    function (url) {
        var /** @type {?} */ resource = this.GetResourceForEndpoint(url);
        return this.context.clearCacheForResource(resource); // Trigger the ADAL token renew
    };
    /**
     * @param {?} url
     * @return {?}
     */
    MsAdalAngular6Service.prototype.acquireToken = /**
     * @param {?} url
     * @return {?}
     */
    function (url) {
        var /** @type {?} */ _this = this; // save outer this for inner function
        var /** @type {?} */ errorMessage;
        return bindCallback(acquireTokenInternal, function (token) {
            if (!token && errorMessage) {
                throw (errorMessage);
            }
            return token;
        })();
        /**
         * @param {?} cb
         * @return {?}
         */
        function acquireTokenInternal(cb) {
            var /** @type {?} */ s = null;
            var /** @type {?} */ resource;
            resource = _this.GetResourceForEndpoint(url);
            _this.context.acquireToken(resource, function (error, tokenOut) {
                if (error) {
                    _this.context.error('Error when acquiring token for resource: ' + resource, error);
                    errorMessage = error;
                    cb(/** @type {?} */ (null));
                }
                else {
                    cb(tokenOut);
                    s = tokenOut;
                }
            });
            return s;
        }
    };
    /**
     * @param {?} url
     * @return {?}
     */
    MsAdalAngular6Service.prototype.getToken = /**
     * @param {?} url
     * @return {?}
     */
    function (url) {
        var /** @type {?} */ resource = this.context.getResourceForEndpoint(url);
        var /** @type {?} */ storage = this.adalConfig.cacheLocation;
        var /** @type {?} */ key;
        if (resource) {
            key = 'adal.access.token.key' + resource;
        }
        else {
            key = 'adal.idtoken';
        }
        if (storage === 'localStorage') {
            return localStorage.getItem(key);
        }
        else {
            return sessionStorage.getItem(key);
        }
    };
    /**
     * @return {?}
     */
    MsAdalAngular6Service.prototype.handleCallback = /**
     * @return {?}
     */
    function () {
        this.context.handleWindowCallback();
    };
    Object.defineProperty(MsAdalAngular6Service.prototype, "userInfo", {
        get: /**
         * @return {?}
         */
        function () {
            return this.context.getCachedUser();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MsAdalAngular6Service.prototype, "accessToken", {
        get: /**
         * @return {?}
         */
        function () {
            return this.context.getCachedToken(this.adalConfig.clientId);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MsAdalAngular6Service.prototype, "isAuthenticated", {
        get: /**
         * @return {?}
         */
        function () {
            return (this.userInfo && this.accessToken) ? true : false;
        },
        enumerable: true,
        configurable: true
    });
    MsAdalAngular6Service.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] },
    ];
    /** @nocollapse */
    MsAdalAngular6Service.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: ['adalConfig',] },] },
    ]; };
    /** @nocollapse */ MsAdalAngular6Service.ngInjectableDef = i0.defineInjectable({ factory: function MsAdalAngular6Service_Factory() { return new MsAdalAngular6Service(i0.inject("adalConfig")); }, token: MsAdalAngular6Service, providedIn: "root" });
    return MsAdalAngular6Service;
}());
export { MsAdalAngular6Service };
function MsAdalAngular6Service_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    MsAdalAngular6Service.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    MsAdalAngular6Service.ctorParameters;
    /** @type {?} */
    MsAdalAngular6Service.prototype.context;
    /** @type {?} */
    MsAdalAngular6Service.prototype.adalConfig;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXMtYWRhbC1hbmd1bGFyNi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbWljcm9zb2Z0LWFkYWwtYW5ndWxhcjYvIiwic291cmNlcyI6WyJzcmMvbXMtYWRhbC1hbmd1bGFyNi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFDQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQWMsWUFBWSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2hELE9BQU8sS0FBSyxPQUFPLE1BQU0sY0FBYyxDQUFDOzs7SUFRdEMsK0JBQTBDO1FBQUEsZUFBVSxHQUFWLFVBQVU7UUFDbEQsRUFBRSxDQUFDLENBQUMsT0FBTyxVQUFVLEtBQUssVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNyQyxJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsRUFBRSxDQUFDO1NBQ2hDO1FBQ0QsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7S0FDdkI7MEJBRVUsb0RBQWlCOzs7OztZQUMxQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztnQkFDekIsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLENBQUMsUUFBUSxDQUFDO2FBQzlDO1lBQ0QsTUFBTSxDQUFDLEVBQUUsQ0FBQzs7Ozs7MEJBR0QsbURBQWdCOzs7OztZQUN6QixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztnQkFDekIsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQzthQUNsRDtZQUNELE1BQU0sQ0FBQyxFQUFFLENBQUM7Ozs7Ozs7O0lBR0wscUNBQUs7Ozs7UUFDVixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxDQUFDOzs7OztJQUdoQixzQ0FBTTs7OztRQUNYLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUM7Ozs7OztJQUdqQixzREFBc0I7Ozs7Y0FBQyxHQUFXO1FBQ3ZDLHFCQUFJLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDcEIsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNSLFFBQVEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3BELEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDZCxRQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUM7YUFDckM7U0FDRjtRQUNELE1BQU0sQ0FBQyxRQUFRLENBQUM7Ozs7OztJQUdYLDBDQUFVOzs7O2NBQUMsR0FBVztRQUMzQixxQkFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2hELE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFDLFFBQVEsQ0FBQyxDQUFDOzs7Ozs7SUFHL0MsNENBQVk7Ozs7Y0FBQyxHQUFXO1FBQzdCLHFCQUFNLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDbkIscUJBQUksWUFBb0IsQ0FBQztRQUV6QixNQUFNLENBQUMsWUFBWSxDQUFDLG9CQUFvQixFQUFFLFVBQUMsS0FBYTtZQUN0RCxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxZQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUMzQixNQUFNLENBQUMsWUFBWSxDQUFDLENBQUM7YUFDdEI7WUFDRCxNQUFNLENBQUMsS0FBSyxDQUFDO1NBQ2QsQ0FBQyxFQUFFLENBQUM7Ozs7O1FBRUwsOEJBQThCLEVBQU87WUFDbkMscUJBQUksQ0FBQyxHQUFXLElBQUksQ0FBQztZQUNyQixxQkFBSSxRQUFnQixDQUFDO1lBQ3JCLFFBQVEsR0FBRyxLQUFLLENBQUMsc0JBQXNCLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFN0MsS0FBSyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLFVBQUMsS0FBYSxFQUFFLFFBQWdCO2dCQUNuRSxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUNWLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLDJDQUEyQyxHQUFHLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFDbkYsWUFBWSxHQUFHLEtBQUssQ0FBQztvQkFDckIsRUFBRSxtQkFBQyxJQUFjLEVBQUMsQ0FBQztpQkFDcEI7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ04sRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUNiLENBQUMsR0FBRyxRQUFRLENBQUM7aUJBQ2Q7YUFDRixDQUFDLENBQUM7WUFDSCxNQUFNLENBQUMsQ0FBQyxDQUFDO1NBQ1Y7Ozs7OztJQUdJLHdDQUFROzs7O2NBQUMsR0FBVztRQUV6QixxQkFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMxRCxxQkFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUM7UUFDOUMscUJBQUksR0FBRyxDQUFDO1FBQ1IsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUNiLEdBQUcsR0FBRyx1QkFBdUIsR0FBRyxRQUFRLENBQUM7U0FDMUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLEdBQUcsR0FBRyxjQUFjLENBQUM7U0FDdEI7UUFDRCxFQUFFLENBQUMsQ0FBQyxPQUFPLEtBQUssY0FBYyxDQUFDLENBQUMsQ0FBQztZQUMvQixNQUFNLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUNsQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sTUFBTSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDcEM7Ozs7O0lBR0gsOENBQWM7OztJQUFkO1FBQ0UsSUFBSSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO0tBQ3JDOzBCQUVVLDJDQUFROzs7OztZQUNqQixNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsQ0FBQzs7Ozs7MEJBRzNCLDhDQUFXOzs7OztZQUNwQixNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQzs7Ozs7MEJBR3BELGtEQUFlOzs7OztZQUN4QixNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Ozs7OztnQkFoSDdELFVBQVUsU0FBQztvQkFDVixVQUFVLEVBQUUsTUFBTTtpQkFDbkI7Ozs7Z0RBSWMsTUFBTSxTQUFDLFlBQVk7OztnQ0FYbEM7O1NBUWEscUJBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiLy8vIDxyZWZlcmVuY2UgcGF0aD0nLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvQHR5cGVzL2FkYWwvaW5kZXguZC50cycvPlxuaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBiaW5kQ2FsbGJhY2sgfSBmcm9tICdyeGpzJztcbmltcG9ydCAqIGFzIGFkYWxMaWIgZnJvbSAnYWRhbC1hbmd1bGFyJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgTXNBZGFsQW5ndWxhcjZTZXJ2aWNlIHtcbiAgcHJpdmF0ZSBjb250ZXh0OiBhZGFsLkF1dGhlbnRpY2F0aW9uQ29udGV4dDtcblxuICBjb25zdHJ1Y3RvcihASW5qZWN0KCdhZGFsQ29uZmlnJykgcHJpdmF0ZSBhZGFsQ29uZmlnOiBhbnkpIHtcbiAgICBpZiAodHlwZW9mIGFkYWxDb25maWcgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIHRoaXMuYWRhbENvbmZpZyA9IGFkYWxDb25maWcoKTtcbiAgICB9IFxuICAgIHRoaXMuY29udGV4dCA9IGFkYWxMaWIuaW5qZWN0KHRoaXMuYWRhbENvbmZpZyk7XG4gICAgdGhpcy5oYW5kbGVDYWxsYmFjaygpO1xuICB9XG5cbiAgcHVibGljIGdldCBMb2dnZWRJblVzZXJFbWFpbCgpIHtcbiAgICBpZiAodGhpcy5pc0F1dGhlbnRpY2F0ZWQpIHtcbiAgICAgIHJldHVybiB0aGlzLmNvbnRleHQuZ2V0Q2FjaGVkVXNlcigpLnVzZXJOYW1lO1xuICAgIH1cbiAgICByZXR1cm4gJyc7XG4gIH1cblxuICBwdWJsaWMgZ2V0IExvZ2dlZEluVXNlck5hbWUoKSB7XG4gICAgaWYgKHRoaXMuaXNBdXRoZW50aWNhdGVkKSB7XG4gICAgICByZXR1cm4gdGhpcy5jb250ZXh0LmdldENhY2hlZFVzZXIoKS5wcm9maWxlLm5hbWU7XG4gICAgfVxuICAgIHJldHVybiAnJztcbiAgfVxuXG4gIHB1YmxpYyBsb2dpbigpIHtcbiAgICB0aGlzLmNvbnRleHQubG9naW4oKTtcbiAgfVxuXG4gIHB1YmxpYyBsb2dvdXQoKSB7XG4gICAgdGhpcy5jb250ZXh0LmxvZ091dCgpO1xuICB9XG5cbiAgcHVibGljIEdldFJlc291cmNlRm9yRW5kcG9pbnQodXJsOiBzdHJpbmcpOiBzdHJpbmcge1xuICAgIGxldCByZXNvdXJjZSA9IG51bGw7XG4gICAgaWYgKHVybCkge1xuICAgICAgcmVzb3VyY2UgPSB0aGlzLmNvbnRleHQuZ2V0UmVzb3VyY2VGb3JFbmRwb2ludCh1cmwpO1xuICAgICAgaWYgKCFyZXNvdXJjZSkge1xuICAgICAgICByZXNvdXJjZSA9IHRoaXMuYWRhbENvbmZpZy5jbGllbnRJZDtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHJlc291cmNlO1xuICB9XG5cbiAgcHVibGljIFJlbmV3VG9rZW4odXJsOiBzdHJpbmcpIHtcbiAgICBsZXQgcmVzb3VyY2UgPSB0aGlzLkdldFJlc291cmNlRm9yRW5kcG9pbnQodXJsKTtcbiAgICByZXR1cm4gdGhpcy5jb250ZXh0LmNsZWFyQ2FjaGVGb3JSZXNvdXJjZShyZXNvdXJjZSk7IC8vIFRyaWdnZXIgdGhlIEFEQUwgdG9rZW4gcmVuZXcgXG4gIH1cblxuICBwdWJsaWMgYWNxdWlyZVRva2VuKHVybDogc3RyaW5nKSB7XG4gICAgY29uc3QgX3RoaXMgPSB0aGlzOyAgIC8vIHNhdmUgb3V0ZXIgdGhpcyBmb3IgaW5uZXIgZnVuY3Rpb25cbiAgICBsZXQgZXJyb3JNZXNzYWdlOiBzdHJpbmc7XG5cbiAgICByZXR1cm4gYmluZENhbGxiYWNrKGFjcXVpcmVUb2tlbkludGVybmFsLCAodG9rZW46IHN0cmluZykgPT4ge1xuICAgICAgaWYgKCF0b2tlbiAmJiBlcnJvck1lc3NhZ2UpIHtcbiAgICAgICAgdGhyb3cgKGVycm9yTWVzc2FnZSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gdG9rZW47XG4gICAgfSkoKTtcblxuICAgIGZ1bmN0aW9uIGFjcXVpcmVUb2tlbkludGVybmFsKGNiOiBhbnkpIHtcbiAgICAgIGxldCBzOiBzdHJpbmcgPSBudWxsO1xuICAgICAgbGV0IHJlc291cmNlOiBzdHJpbmc7XG4gICAgICByZXNvdXJjZSA9IF90aGlzLkdldFJlc291cmNlRm9yRW5kcG9pbnQodXJsKTtcblxuICAgICAgX3RoaXMuY29udGV4dC5hY3F1aXJlVG9rZW4ocmVzb3VyY2UsIChlcnJvcjogc3RyaW5nLCB0b2tlbk91dDogc3RyaW5nKSA9PiB7XG4gICAgICAgIGlmIChlcnJvcikge1xuICAgICAgICAgIF90aGlzLmNvbnRleHQuZXJyb3IoJ0Vycm9yIHdoZW4gYWNxdWlyaW5nIHRva2VuIGZvciByZXNvdXJjZTogJyArIHJlc291cmNlLCBlcnJvcik7XG4gICAgICAgICAgZXJyb3JNZXNzYWdlID0gZXJyb3I7XG4gICAgICAgICAgY2IobnVsbCBhcyBzdHJpbmcpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGNiKHRva2VuT3V0KTtcbiAgICAgICAgICBzID0gdG9rZW5PdXQ7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgICAgcmV0dXJuIHM7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIGdldFRva2VuKHVybDogc3RyaW5nKTogc3RyaW5nIHtcblxuICAgIGNvbnN0IHJlc291cmNlID0gdGhpcy5jb250ZXh0LmdldFJlc291cmNlRm9yRW5kcG9pbnQodXJsKTtcbiAgICBjb25zdCBzdG9yYWdlID0gdGhpcy5hZGFsQ29uZmlnLmNhY2hlTG9jYXRpb247XG4gICAgbGV0IGtleTtcbiAgICBpZiAocmVzb3VyY2UpIHtcbiAgICAgIGtleSA9ICdhZGFsLmFjY2Vzcy50b2tlbi5rZXknICsgcmVzb3VyY2U7XG4gICAgfSBlbHNlIHtcbiAgICAgIGtleSA9ICdhZGFsLmlkdG9rZW4nO1xuICAgIH1cbiAgICBpZiAoc3RvcmFnZSA9PT0gJ2xvY2FsU3RvcmFnZScpIHtcbiAgICAgIHJldHVybiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShrZXkpO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShrZXkpO1xuICAgIH1cbiAgfVxuXG4gIGhhbmRsZUNhbGxiYWNrKCkge1xuICAgIHRoaXMuY29udGV4dC5oYW5kbGVXaW5kb3dDYWxsYmFjaygpO1xuICB9XG5cbiAgcHVibGljIGdldCB1c2VySW5mbygpIHtcbiAgICByZXR1cm4gdGhpcy5jb250ZXh0LmdldENhY2hlZFVzZXIoKTtcbiAgfVxuXG4gIHB1YmxpYyBnZXQgYWNjZXNzVG9rZW4oKSB7XG4gICAgcmV0dXJuIHRoaXMuY29udGV4dC5nZXRDYWNoZWRUb2tlbih0aGlzLmFkYWxDb25maWcuY2xpZW50SWQpO1xuICB9XG5cbiAgcHVibGljIGdldCBpc0F1dGhlbnRpY2F0ZWQoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuICh0aGlzLnVzZXJJbmZvICYmIHRoaXMuYWNjZXNzVG9rZW4pID8gdHJ1ZSA6IGZhbHNlO1xuICB9XG59Il19