/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from "@angular/core";
import { MsAdalAngular6Service } from "./ms-adal-angular6.service";
var AuthenticationGuard = /** @class */ (function () {
    function AuthenticationGuard(adalSvc) {
        this.adalSvc = adalSvc;
    }
    /**
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    AuthenticationGuard.prototype.canActivate = /**
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    function (route, state) {
        if (this.adalSvc.isAuthenticated) {
            return true;
        }
        else {
            this.adalSvc.login();
            return false;
        }
    };
    /**
     * @param {?} childRoute
     * @param {?} state
     * @return {?}
     */
    AuthenticationGuard.prototype.canActivateChild = /**
     * @param {?} childRoute
     * @param {?} state
     * @return {?}
     */
    function (childRoute, state) {
        return this.canActivate(childRoute, state);
    };
    AuthenticationGuard.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    AuthenticationGuard.ctorParameters = function () { return [
        { type: MsAdalAngular6Service, },
    ]; };
    return AuthenticationGuard;
}());
export { AuthenticationGuard };
function AuthenticationGuard_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    AuthenticationGuard.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    AuthenticationGuard.ctorParameters;
    /** @type {?} */
    AuthenticationGuard.prototype.adalSvc;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGljYXRpb24tZ3VhcmQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9taWNyb3NvZnQtYWRhbC1hbmd1bGFyNi8iLCJzb3VyY2VzIjpbInNyYy9hdXRoZW50aWNhdGlvbi1ndWFyZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUzQyxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQzs7SUFJL0QsNkJBQW9CLE9BQThCO1FBQTlCLFlBQU8sR0FBUCxPQUFPLENBQXVCO0tBQUs7Ozs7OztJQUVoRCx5Q0FBVzs7Ozs7Y0FBQyxLQUE2QixFQUFFLEtBQTBCO1FBQ3hFLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztZQUMvQixNQUFNLENBQUMsSUFBSSxDQUFDO1NBQ2Y7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDckIsTUFBTSxDQUFDLEtBQUssQ0FBQztTQUNoQjs7Ozs7OztJQUdFLDhDQUFnQjs7Ozs7Y0FBQyxVQUFrQyxFQUFFLEtBQTBCO1FBQ2xGLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQzs7O2dCQWRsRCxVQUFVOzs7O2dCQUZGLHFCQUFxQjs7OEJBRjlCOztTQUthLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgQ2FuQWN0aXZhdGUsIENhbkFjdGl2YXRlQ2hpbGQsIEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIFJvdXRlclN0YXRlU25hcHNob3QgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgTXNBZGFsQW5ndWxhcjZTZXJ2aWNlIH0gZnJvbSBcIi4vbXMtYWRhbC1hbmd1bGFyNi5zZXJ2aWNlXCI7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBBdXRoZW50aWNhdGlvbkd1YXJkIGltcGxlbWVudHMgQ2FuQWN0aXZhdGUsIENhbkFjdGl2YXRlQ2hpbGQge1xuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYWRhbFN2YzogTXNBZGFsQW5ndWxhcjZTZXJ2aWNlKSB7IH1cblxuICAgIHB1YmxpYyBjYW5BY3RpdmF0ZShyb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3QpOiBib29sZWFuIHtcbiAgICAgICAgaWYgKHRoaXMuYWRhbFN2Yy5pc0F1dGhlbnRpY2F0ZWQpIHtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5hZGFsU3ZjLmxvZ2luKCk7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgY2FuQWN0aXZhdGVDaGlsZChjaGlsZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5jYW5BY3RpdmF0ZShjaGlsZFJvdXRlLCBzdGF0ZSk7XG4gICAgfVxufSJdfQ==