/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { NgModule } from '@angular/core';
import { MsAdalAngular6Service } from './ms-adal-angular6.service';
var MsAdalAngular6Module = /** @class */ (function () {
    function MsAdalAngular6Module() {
    }
    /**
     * @param {?} adalConfig
     * @return {?}
     */
    MsAdalAngular6Module.forRoot = /**
     * @param {?} adalConfig
     * @return {?}
     */
    function (adalConfig) {
        return {
            ngModule: MsAdalAngular6Module,
            providers: [MsAdalAngular6Service, { provide: 'adalConfig', useValue: adalConfig }]
        };
    };
    MsAdalAngular6Module.decorators = [
        { type: NgModule, args: [{
                    imports: [],
                    declarations: [],
                    exports: []
                },] },
    ];
    return MsAdalAngular6Module;
}());
export { MsAdalAngular6Module };
function MsAdalAngular6Module_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    MsAdalAngular6Module.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    MsAdalAngular6Module.ctorParameters;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXMtYWRhbC1hbmd1bGFyNi5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9taWNyb3NvZnQtYWRhbC1hbmd1bGFyNi8iLCJzb3VyY2VzIjpbInNyYy9tcy1hZGFsLWFuZ3VsYXI2Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUV6QyxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQzs7Ozs7Ozs7SUFRMUQsNEJBQU87Ozs7SUFBZCxVQUFlLFVBQWU7UUFDNUIsTUFBTSxDQUFDO1lBQ0wsUUFBUSxFQUFFLG9CQUFvQjtZQUM5QixTQUFTLEVBQUUsQ0FBQyxxQkFBcUIsRUFBRSxFQUFFLE9BQU8sRUFBRSxZQUFZLEVBQUUsUUFBUSxFQUFFLFVBQVUsRUFBRSxDQUFDO1NBQ3BGLENBQUM7S0FDSDs7Z0JBWEYsUUFBUSxTQUFDO29CQUNSLE9BQU8sRUFBRSxFQUFFO29CQUNYLFlBQVksRUFBRSxFQUFFO29CQUNoQixPQUFPLEVBQUUsRUFBRTtpQkFDWjs7K0JBUkQ7O1NBU2Esb0JBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE1vZHVsZVdpdGhQcm92aWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE1zQWRhbEFuZ3VsYXI2U2VydmljZSB9IGZyb20gJy4vbXMtYWRhbC1hbmd1bGFyNi5zZXJ2aWNlJztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW10sXG4gIGRlY2xhcmF0aW9uczogW10sXG4gIGV4cG9ydHM6IFtdXG59KVxuZXhwb3J0IGNsYXNzIE1zQWRhbEFuZ3VsYXI2TW9kdWxlIHsgXG4gIHN0YXRpYyBmb3JSb290KGFkYWxDb25maWc6IGFueSk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xuICAgIHJldHVybiB7XG4gICAgICBuZ01vZHVsZTogTXNBZGFsQW5ndWxhcjZNb2R1bGUsXG4gICAgICBwcm92aWRlcnM6IFtNc0FkYWxBbmd1bGFyNlNlcnZpY2UsIHsgcHJvdmlkZTogJ2FkYWxDb25maWcnLCB1c2VWYWx1ZTogYWRhbENvbmZpZyB9XVxuICAgIH07XG4gIH1cbn0iXX0=